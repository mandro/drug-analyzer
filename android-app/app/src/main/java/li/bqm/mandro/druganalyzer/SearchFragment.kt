package li.bqm.mandro.druganalyzer

import android.content.Context
import android.support.v4.app.ListFragment
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import li.bqm.mandro.druganalyzer.adapters.SearchAdapter
import li.bqm.mandro.druganalyzer.models.SearchItem
import org.json.JSONArray

class SearchFragment : ListFragment() {

    private var mListAdapter: SearchAdapter? = null
    private var mCallback: IActivityCallback? = null
    private var mQuery: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mListAdapter = SearchAdapter(requireContext())

        listAdapter = mListAdapter

        return inflater.inflate(R.layout.allergies_fragment, null)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IActivityCallback) {
            mCallback = context
        } else {
            throw RuntimeException(context.toString() + " must implement IActivityCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onResume() {
        super.onResume()
        mCallback!!.showSearchButton(true)
        mCallback!!.setTitle(null)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        val fragment = DetailsFragment.newInstance(mListAdapter!!.getItem(position).getId())
        mCallback!!.loadFragment(fragment)
    }

    fun search(query: String) {
        mQuery = query
        Log.d("SearchFragment", "Query: $query")
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(requireContext())
        val url = "http://api.druganalyzer.mandro.bqm.li:5000/drugs?query=$query"
        // Request a string response from the provided URL.
        val jsonObjReq = JsonArrayRequest(Request.Method.GET,
                url, null,
                Response.Listener<JSONArray> {
                    response -> Log.d("SearchFragment", response.toString())

                    if (response.length() > 0) {
                        mListAdapter?.clear()
                        for (i in 0 until response.length()) {
                            val jsonObject = response.getJSONObject(i)
                            val searchItem = SearchItem(jsonObject.getString("id"), jsonObject.getString("title"))
                            mListAdapter?.add(searchItem)
                        }
                    }
                },
                Response.ErrorListener {
                    error -> Log.d("DetailsFragment", error.toString())
                })

        // Add the request to the RequestQueue.
        queue.add(jsonObjReq)
    }
}
