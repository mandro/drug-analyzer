package li.bqm.mandro.druganalyzer.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import li.bqm.mandro.druganalyzer.AllergyUtil
import li.bqm.mandro.druganalyzer.models.Substance

class AllergiesStoreAdapter(context: Context) : ArrayAdapter<Substance>(context, android.R.layout.simple_list_item_1, ArrayList<Substance>()) {

    private val mAllergyList: ArrayList<Substance> = ArrayList()

    init {
        load()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View = convertView ?: View.inflate (context, android.R.layout.simple_list_item_1, null)

        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = mAllergyList[position].getName()
        return view
    }

    override fun add(item: Substance) {
        super.add(item)
        mAllergyList.add(item)
        save()
    }

    override fun addAll(vararg items: Substance?) {
        super.addAll(*items)
        for (item in items) {
            add(item!!)
        }
    }

    override fun addAll(collection: MutableCollection<out Substance>?) {
        super.addAll(collection)
        mAllergyList.addAll(collection!!.asIterable())
        save()
    }

    override fun remove(`object`: Substance?) {
        super.remove(`object`)
        mAllergyList.remove(`object`)
        save()
    }

    override fun clear() {
        super.clear()
        mAllergyList.clear()
    }

    fun reload() {
        clear()
        load()
    }

    private fun load() {
        addAll(AllergyUtil.restoreList(context))
    }

    private fun save() {
        AllergyUtil.saveList(context, mAllergyList)
    }
}