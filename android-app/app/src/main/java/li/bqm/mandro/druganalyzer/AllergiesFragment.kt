package li.bqm.mandro.druganalyzer

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.ListFragment
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.*
import li.bqm.mandro.druganalyzer.adapters.AllergiesStoreAdapter
import li.bqm.mandro.druganalyzer.models.Substance
import java.util.function.Predicate


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AllergiesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AllergiesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class AllergiesFragment : ListFragment() {

    private var mListAdapter: AllergiesStoreAdapter? = null
    private var mCallback: IActivityCallback? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mListAdapter = AllergiesStoreAdapter(requireContext())

        listAdapter = mListAdapter

        val view = inflater.inflate(R.layout.allergies_fragment, null)
        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        val dopingSwitch = view.findViewById<Switch>(R.id.doping_preference)

        // FAB click handler
        fab.setOnClickListener {
            val addAllergyFragment = AddAllergyFragment.newInstance()
            mCallback?.loadFragment(addAllergyFragment)
        }

        dopingSwitch.isChecked = AllergyUtil.getDopingInformationPreference(requireContext())
        dopingSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            AllergyUtil.setDopingInformationPreference(requireContext(), isChecked)
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IActivityCallback) {
            mCallback = context
        } else {
            throw RuntimeException(context.toString() + " must implement IActivityCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onResume() {
        super.onResume()
        mCallback!!.showSearchButton(false)
        mCallback!!.setTitle(R.string.allergies_title)
        mListAdapter?.reload()
        listView.setOnItemLongClickListener { parent, itemView, position, id ->
            mListAdapter?.remove(mListAdapter?.getItem(position))

            true
        }
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        Toast.makeText(requireContext(), R.string.allergy_remove_hint, Toast.LENGTH_SHORT).show()
    }
}
