package li.bqm.mandro.druganalyzer.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import li.bqm.mandro.druganalyzer.models.SearchItem

class SearchAdapter(context: Context) : ArrayAdapter<SearchItem>(context, android.R.layout.simple_list_item_1, ArrayList<SearchItem>()) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View = convertView ?: View.inflate (context, android.R.layout.simple_list_item_1, null)

        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = getItem(position).getLabel()

        return view
    }
}
