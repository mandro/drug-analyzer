package li.bqm.mandro.druganalyzer.models

class SearchItem(private var id: String, private var label: String) {

    fun getId(): String {
        return id
    }

    fun getLabel(): String {
        return label
    }

    fun setLabel(label: String) {
        this.label = label
    }
}
