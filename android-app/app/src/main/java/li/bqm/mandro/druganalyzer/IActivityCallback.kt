package li.bqm.mandro.druganalyzer

import android.support.v4.app.Fragment

interface IActivityCallback {
    fun loadFragment(fragment: Fragment, addToBackStack: Boolean = true)
    fun showSearchButton(show: Boolean)
    fun setTitle(resId: Int?)
}
