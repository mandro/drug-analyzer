package li.bqm.mandro.druganalyzer.models

class Substance(private var id: String, private var name: String, private var prohibited: Int = 0) {

    fun getId(): String {
        return id
    }

    fun getName(): String {
        return name
    }

    fun getProhibited(): Int {
        return prohibited
    }

    fun setName(name: String) {
        this.name = name
    }

    fun setProhibited(prohibited: Int) {
        this.prohibited = prohibited
    }
}
