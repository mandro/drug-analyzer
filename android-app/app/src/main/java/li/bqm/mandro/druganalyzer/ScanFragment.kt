package li.bqm.mandro.druganalyzer

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.Result

import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScanFragment : Fragment(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null
    private var mDetailsFragment: DetailsFragment? = null
    private var mCallback: IActivityCallback? = null
    private val MY_PERMISSIONS_REQUEST_CAMERA = 10

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.scan_fragment, null)
        val cameraFrame = view.findViewById(R.id.camera_frame) as ViewGroup
        mScannerView = ZXingScannerView(requireContext())
        cameraFrame.addView(mScannerView)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IActivityCallback) {
            mCallback = context
        } else {
            throw RuntimeException(context.toString() + " must implement IActivityCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onResume() {
        super.onResume()
        mCallback!!.showSearchButton(true)
        mCallback!!.setTitle(null)

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            ActivityCompat.requestPermissions(requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_REQUEST_CAMERA)
        } else {
            // Permission has already been granted
            if (activity?.findViewById<ViewGroup>(R.id.details_frame)?.childCount == 0) {
                startScanner()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        stopScanner()
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        Log.v("ScanFragment", rawResult.text) // Prints scan results
        Log.v("ScanFragment", rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode, pdf417 etc.)

        mDetailsFragment = DetailsFragment.newInstance(rawResult.text)
        mCallback!!.loadFragment(mDetailsFragment!!)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay!
                    startScanner()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

        // Add other 'when' lines to check for other
        // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    fun startScanner() {
        mScannerView?.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView?.startCamera()          // Start camera on resume
        mScannerView?.setAutoFocus(true)
    }

    private fun resumeScanner() {
        mScannerView?.resumeCameraPreview(this)
    }

    fun stopScanner() {
        mScannerView?.stopCamera()
    }
}
