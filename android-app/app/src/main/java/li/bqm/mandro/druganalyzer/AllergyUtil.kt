package li.bqm.mandro.druganalyzer

import android.content.Context
import android.content.Context.MODE_PRIVATE
import li.bqm.mandro.druganalyzer.models.Substance

class AllergyUtil {

    companion object {
        private val MY_PREFS_NAME: String = "li.bqm.mandro.druganalyzer"

        fun restoreList(context: Context): ArrayList<Substance> {
            val prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE)

            val allergyList = ArrayList<Substance>()
            val csvList = prefs.getString("allergyList", "")
            val items = csvList.split(",")
            for (item: String in items) {
                if (!item.isEmpty()) {
                    val data = item.split(";")
                    allergyList.add(Substance(data[0], data[1]))
                }
            }
            return allergyList
        }

        fun saveList(context: Context, allergyList: List<Substance>) {
            val csvList = StringBuilder()
            for (s in allergyList) {
                csvList.append(s.getId() + ";" + s.getName())
                csvList.append(",")
            }

            val editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit()
            editor.putString("allergyList", csvList.toString())
            editor.apply()
        }

        fun appendList(context: Context, substance: Substance) {
            val prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE)
            val csvList = prefs.getString("allergyList", "") + "," + substance.getId() + ";" + substance.getName()
            val editor = prefs.edit()
            editor.putString("allergyList", csvList)
            editor.apply()
        }

        fun getDopingInformationPreference(context: Context): Boolean {
            val prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE)
            return prefs.getBoolean("dopingInfoPref", true)
        }

        fun setDopingInformationPreference(context: Context, value: Boolean) {
            val editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit()
            editor.putBoolean("dopingInfoPref", value)
            editor.apply()
        }
    }
}
