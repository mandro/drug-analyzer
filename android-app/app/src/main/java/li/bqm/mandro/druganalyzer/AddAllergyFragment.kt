package li.bqm.mandro.druganalyzer

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.text.Editable
import android.util.Log
import android.widget.ListView
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import li.bqm.mandro.druganalyzer.adapters.AllergiesAdapter
import li.bqm.mandro.druganalyzer.models.Substance
import org.json.JSONArray


class AddAllergyFragment : Fragment() {

    private var mCallback: IActivityCallback? = null
    private var mAllergenEditText: EditText? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.add_allergy_fragment, container, false)

        // Set up ListView
        val listView = view.findViewById<ListView>(R.id.allergenListView)
        listView.setOnItemClickListener{adapterView, view, i, l ->
            // Determine the ID (the one that's in the API) of the clicked item
            val substance = (adapterView.adapter.getItem(i) as Substance)
            Log.d("addAllergyFragment", id.toString())

            // Add new allergen to the user's list and return
            AllergyUtil.appendList(requireContext(), substance)
            requireActivity().onBackPressed()
        }

        // Set up Volley
        val queue = Volley.newRequestQueue(requireContext())
        val url = "http://api.druganalyzer.mandro.bqm.li:5000/substances?query="

        // Fetch EditText and set up listener
        mAllergenEditText = view.findViewById<EditText>(R.id.allergenEditText)
        val allergiesAdapter = AllergiesAdapter(requireContext())
        val allergenListView = view.findViewById<ListView>(R.id.allergenListView)
        allergenListView.adapter = allergiesAdapter
        mAllergenEditText?.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                val query = s.toString()
                val jsonArrayRequest = JsonArrayRequest(url + query,
                        Response.Listener<JSONArray> { response ->
                            Log.d("API /substances", response.length().toString())
                            allergiesAdapter.clear()
                            if (response.length() > 0) {
                                for (i in 0 until response.length()) {
                                    val item = response.getJSONObject(i)
                                    allergiesAdapter.add(Substance(item.getString("id"),
                                                                   item.getString("canonical_name"),
                                                                   item.getString("prohibited").toInt()))
                                }
                            }
                        },
                        Response.ErrorListener { error ->
                            Log.d("API /substances", error.message)
                        })
                queue.add(jsonArrayRequest)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IActivityCallback) {
            mCallback = context
        } else {
            throw RuntimeException(context.toString() + " must implement IActivityCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onResume() {
        super.onResume()
        mCallback!!.showSearchButton(false)
        mCallback!!.setTitle(R.string.add_allergies_title)
        mAllergenEditText?.requestFocus()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment AddAllergyFragment.
         */
        @JvmStatic
        fun newInstance() = AddAllergyFragment()
    }
}
