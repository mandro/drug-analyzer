package li.bqm.mandro.druganalyzer

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IActivityCallback {

    private var mScanFragment: ScanFragment = ScanFragment()
    private var mAllergiesFragment: AllergiesFragment = AllergiesFragment()
    private var mSearchActionItem: MenuItem? = null
    private var mCurrentFragment: Fragment = mScanFragment

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_scanner -> {
                loadFragment(mScanFragment, false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_allergies -> {

                loadFragment(mAllergiesFragment, false)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()

        if (findViewById<ViewGroup>(R.id.content_frame).childCount == 0) {
            loadFragment(mScanFragment, false)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (supportFragmentManager.backStackEntryCount == 0) {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    override fun loadFragment(fragment: Fragment, addToBackStack: Boolean) {
        if (fragment.isAdded) return

        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.content_frame, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(null)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        transaction.commit()
        mCurrentFragment = fragment
    }

    override fun showSearchButton(show: Boolean) {
        mSearchActionItem?.isVisible = show
    }

    override fun setTitle(resId: Int?) {
        if (resId == null) {
            supportActionBar?.setTitle(R.string.app_name)
        } else {
            supportActionBar?.setTitle(resId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_buttons, menu)

        // Define the expand / collapse listener
        val expandListener = object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                onBackPressed()
                return true // Return true to collapse action view
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                loadFragment(SearchFragment())
                return true // Return true to expand action view
            }
        }

        // Get the MenuItem for the action item
        mSearchActionItem = menu?.findItem(R.id.action_search)

        // Assign the listener to that action item
        mSearchActionItem?.setOnActionExpandListener(expandListener)

        // Define the search text change listener
        val searchTextChangeListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                val query: String = newText ?: ""
                if (mCurrentFragment is SearchFragment && mCurrentFragment.isAdded) {
                    (mCurrentFragment as SearchFragment).search(query)
                }
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                // Send search query to API
                Log.d("SearchView", "Sending query to API: $query")


                // TODO: Show search results

                return true
            }
        }

        // Get the SearchView
        val searchView = mSearchActionItem?.actionView as SearchView

        // Assign the listener to that view
        searchView.setOnQueryTextListener(searchTextChangeListener)

        return true
    }
}
