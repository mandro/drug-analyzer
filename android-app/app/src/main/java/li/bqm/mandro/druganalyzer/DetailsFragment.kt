package li.bqm.mandro.druganalyzer

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import com.android.volley.toolbox.JsonObjectRequest
import li.bqm.mandro.druganalyzer.models.Substance
import org.json.JSONException
import org.w3c.dom.Text


private const val ARG_GTIN = "gtin"

class DetailsFragment : Fragment() {
    private var mGtin: String? = null

    private var mCallback: IActivityCallback? = null
    private var mLoadingView: ProgressBar? = null
    private var mErrorView: ViewGroup? = null
    private var mDetailsView: ViewGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mGtin = it.getString(ARG_GTIN)
        }
        if (mGtin == null) throw NullPointerException("mGtin may not be null.")

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(requireContext())
        val url = "http://api.druganalyzer.mandro.bqm.li:5000/drugs/$mGtin"

        // Request a string response from the provided URL.
        val jsonObjReq = JsonObjectRequest(Request.Method.GET,
                url, null,
                Response.Listener<JSONObject> {
                    response -> Log.d("DetailsFragment", response.toString())
                    mLoadingView?.visibility = View.GONE

                    mDetailsView?.findViewById<TextView>(R.id.title)?.text = response.getString("title")
                    mDetailsView?.findViewById<TextView>(R.id.auth_holder)?.text = response.getString("authHolder")

                    try {
                        val ingredientsContainerView = mDetailsView?.findViewById<ViewGroup>(R.id.ingredient_container)

                        val ingredients = response.getJSONArray("substances")

                        if (ingredients.length() > 0) {
                            ingredientsContainerView!!.findViewById<TextView>(R.id.no_ingredients).visibility = View.GONE
                            for (i in 0..ingredients.length()) {
                                val jsonObject = ingredients.getJSONObject(i)
                                val ingredient = Substance(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getInt("prohibited"))
                                val view = createIngredientView(ingredient, ingredientsContainerView)
                                ingredientsContainerView.addView(view)
                            }
                        }
                    } catch (e: JSONException) {
                        // ignored
                    }

                    mDetailsView?.findViewById<TextView>(R.id.auth_no)?.text = response.getString("authNrs")
                    mDetailsView?.findViewById<TextView>(R.id.atc)?.text = response.getString("atcCode")
                    mDetailsView?.findViewById<TextView>(R.id.gtin)?.text = mGtin

                    mDetailsView?.visibility = View.VISIBLE
                },
                Response.ErrorListener {
                    error -> Log.d("DetailsFragment", error.toString())
                    if (error.networkResponse?.statusCode == 404) {
                        mErrorView?.findViewById<TextView>(R.id.error_text)?.setText(R.string.error_not_found)
                    }
                    mLoadingView?.visibility = View.GONE
                    mErrorView?.visibility = View.VISIBLE
                })

        // Add the request to the RequestQueue.
        queue.add(jsonObjReq)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.details_fragment, container, false)
        mLoadingView = view.findViewById(R.id.progress_bar)
        mErrorView = view.findViewById(R.id.error)
        mDetailsView = view.findViewById(R.id.details)
        return view
    }

    private fun createIngredientView(substance: Substance, container: ViewGroup): View {
        val layout = requireActivity().layoutInflater.inflate(R.layout.substance_item, container, false)
        layout.findViewById<TextView>(R.id.substance_name).text = substance.getName()

        // Show allergy information
        val allergies = AllergyUtil.restoreList(requireContext())

        var iconResAllergies = R.drawable.ic_emoji_happy_green_24dp
        for (i in 0 until allergies.size) {
            if (allergies[i].getId() == substance.getId()) {
                // Show red smiley and set text color to red
                iconResAllergies = R.drawable.ic_emoji_x_eyes_red_24dp
                layout.findViewById<TextView>(R.id.substance_name).setTextColor(Color.RED)
                break
            }
        }
        layout.findViewById<ImageView>(R.id.substance_icon_allergy).setImageResource(iconResAllergies)

        // Show doping information if enabled
        if (AllergyUtil.getDopingInformationPreference(requireContext())) {
            layout.findViewById<ImageView>(R.id.substance_icon_doping).visibility = View.VISIBLE
            val iconResDoping = when {
                substance.getProhibited() == 1 -> R.drawable.ic_doping_maybe_24dp
                substance.getProhibited() == 2 -> R.drawable.ic_doping_no_24dp
                else -> R.drawable.ic_check_green_24dp
            }
            layout.findViewById<ImageView>(R.id.substance_icon_doping).setImageResource(iconResDoping)
        }
        return layout
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IActivityCallback) {
            mCallback = context
        } else {
            throw RuntimeException(context.toString() + " must implement IActivityCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onResume() {
        super.onResume()
        mCallback!!.showSearchButton(false)
        mCallback!!.setTitle(null)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param gtin Global Trade Item Number.
         * @return A new instance of fragment DetailsFragment.
         */
        @JvmStatic
        fun newInstance(gtin: String) =
                DetailsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_GTIN, gtin)
                    }
                }
    }
}
