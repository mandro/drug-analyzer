"""Our own API, which provides an improved version of the swissmedic dataset."""
from flask import Flask, jsonify, request
import mysql.connector

from secrets import mysql_creds

app = Flask(__name__)


def _fuzzy_search(table, column_select, column_compare, parameter, column_order):
    """Helper function to perform fuzzy string matching <parameter> to <table>.<column_compare>.
    Returns the columns given in <column_select>, ordered by <column_order>.
    """
    # Ensure that the parameter was provided.
    query = request.args.get(parameter)
    if not query:
        return jsonify([])

    query = '%' + query + '%'

    db = mysql.connector.connect(**mysql_creds)
    cursor = db.cursor(dictionary=True)
    cursor.execute('''SELECT {column_select} FROM {table}
                      WHERE {column_compare} LIKE %s
                      ORDER BY {column_order}'''.format(column_select=column_select,
                                                        table=table,
                                                        column_compare=column_compare,
                                                        column_order=column_order),
                                                        params=[query,])

    return jsonify(cursor.fetchall())


@app.route('/drugs')
def drugs_by_name():
    """Find drugs based on approximate name.
    Param: query
    """
    return _fuzzy_search('swissmedic_raw', '*', 'title', 'query', 'title')


@app.route('/drugs/<string:_id>')
def drug_by_gtin(_id):
    """Find a specific drug by its GTIN or internal id
    Param: id
    """
    import zeep

    db = mysql.connector.connect(**mysql_creds)
    cursor = db.cursor(dictionary=True, buffered=True)
    cursor.execute('SELECT * FROM swissmedic_raw WHERE id = %s ORDER BY title', params=[_id])

    if (cursor.rowcount == 0):
        WSDL = 'https://refdatabase.refdata.ch/Service/Article.asmx?WSDL'
        client = zeep.Client(wsdl=WSDL)

        operation_proxy = client.service.Download(TYPE='GTIN', ATYPE='ALL', TERM=_id)

        # Check for success of operation
        if not operation_proxy:
            return (jsonify({'error': 'refdata.ch query failed.'}), 500, None)

        if operation_proxy['RESULT']['OK_ERROR'] != 'OK':
            return (jsonify({'error': 'refdata.ch query returned an error.'}), 500, None)

        if operation_proxy['RESULT']['NBR_RECORD'] == 0:
            return (jsonify({'error': 'GTIN not found.'}), 404, None)

        if operation_proxy['RESULT']['NBR_RECORD'] > 1:
            return (jsonify({'error': 'Ambiguous GTIN.'}), 500, None)

        # swissmedic ID consists of the first 5 digits of the auth number
        auth_nr = operation_proxy['ITEM'][0]['SWMC_AUTHNR']
        swissmedic_id = '%' + auth_nr[:5] + '%'

        db = mysql.connector.connect(**mysql_creds)
        cursor = db.cursor(dictionary=True, buffered=True)
        cursor.execute('SELECT * FROM swissmedic_raw WHERE authNrs LIKE %s ORDER BY title', params=[swissmedic_id,])
    drug_data = cursor.fetchone()

    # Add substance data
    cursor.execute('''SELECT drug_substances.substance_id AS id, substances.canonical_name AS name, substances.prohibited AS prohibited FROM drug_substances
                      JOIN substances ON substances.id = drug_substances.substance_id
                      WHERE drug_substances.drug_id = %s
                      ORDER BY name''', params=[drug_data['id'],])
    drug_data['substances'] = cursor.fetchall()

    return jsonify(drug_data)


@app.route('/substances/<int:_id>')
def substance_by_id(_id):
    """Find a specific substance by its id.
    Param: id
    """
    db = mysql.connector.connect(**mysql_creds)
    cursor = db.cursor(dictionary=True)
    cursor.execute('SELECT * FROM substances WHERE id=%s ORDER BY canonical_name', params=[_id,])

    return jsonify(cursor.fetchall())


@app.route('/substances')
def substances_by_name():
    """Find substances based on approximate name.
    Param: query
    """
    return _fuzzy_search('substances', '*', 'canonical_name', 'query', 'canonical_name')
