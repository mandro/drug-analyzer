# Readme
This is a two-person project which first saw the light of day at the [HackZurich](http://hackzurich.ch) hackathon in 2018. We came up with an Android application, which allows people to look up information on any pharmaceutical or therapeutical product sold in Switzerland, simply by scanning the barcode on the packaging with their smartphones.

## Android App
We used the Android SDK and the Kotlin programming language, as well as two additional libraries:
* __Volley__ is an open-source library managed by Google. It makes working with JSON APIs and Kotlin very pleasant. [https://github.com/google/volley](https://github.com/google/volley)
* [https://github.com/dm77/barcodescanner](https://github.com/dm77/barcodescanner) is a really neat barcode scanner library.

## API
### Technology
* __Flask__: A lightweight, small Python web framework. [http://flask.pocoo.org/](http://flask.pocoo.org/)
* __Zeep__: SOAP Client for Python. [https://python-zeep.readthedocs.io](https://python-zeep.readthedocs.io)
* MySQL for the database backend
### Data Sources
* __Swissmedic__: The Swiss Agency for Therapeutic Products provides an XML database of every single therapeutic product that is sold in Switzerland. The file weighs in at a whopping 1.1 gigabytes, so that was kinda interesting... [http://download.swissmedicinfo.ch](http://download.swissmedicinfo.ch)
* __refdata__: A foundation that, amongst other things, is in charge of assigning GTIN / EAN-13 numbers in the Swiss healthcare system. [https://refdatabase.refdata.ch/](https://refdatabase.refdata.ch/)
* __AXA__: Last but not least, our sponsor also provided us with several REST APIs, which facilitated the process of exploring the possibilities in electronic healthcare.

### Endpoints
#### `GET /drugs`
#### `GET /drugs/<id>`
#### `GET /substances`
#### `GET /substances/<id>`